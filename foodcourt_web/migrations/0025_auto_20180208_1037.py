# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-02-08 10:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('foodcourt_web', '0024_remove_tableordermapping_table_no'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='order_amount',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='order',
            name='order_no',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
