# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2017-12-21 10:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('foodcourt_web', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='icon',
            field=models.ImageField(default=1, upload_to=b''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='item_image',
            field=models.ImageField(default=11, upload_to=b''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='bill',
            name='bill_no',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='order',
            name='order_no',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='table',
            name='table_no',
            field=models.IntegerField(),
        ),
    ]
