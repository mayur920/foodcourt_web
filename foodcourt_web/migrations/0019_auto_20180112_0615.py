# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-12 06:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('foodcourt_web', '0018_itemordermapping_quantity'),
    ]

    operations = [
        migrations.AlterField(
            model_name='table',
            name='table_no',
            field=models.CharField(max_length=100),
        ),
    ]
