from django.contrib import admin
from foodcourt_web.models import Restaurant,Category,Item,Table,Order,Bill,ItemOrderMapping,Section,TableOrderMapping,CategoryType,BillReport

admin.site.register(BillReport)

class RestaurantAdmin(admin.ModelAdmin):
	list_display = ('restaurant_name', 'restaurant_address', 'restaurant_contact', 'admin')
	list_filter = ('restaurant_name', 'restaurant_address', 'restaurant_contact', 'admin')
	search_fields = ('restaurant_name', 'restaurant_address')

admin.site.register(Restaurant, RestaurantAdmin)

class ItemAdmin(admin.ModelAdmin):
	list_display = ('id','item_name', 'item_code','item_rate', 'category', 'restaurant')
	list_filter = ('id','item_name','item_code', 'item_rate', 'category', 'restaurant')
	search_fields = ('item_name', 'item_code')

admin.site.register(Item, ItemAdmin)

class CategoryAdmin(admin.ModelAdmin):
	list_display = ('id','category_name', 'restaurant')
	list_filter = ('id','category_name', 'restaurant')
	search_fields = ('category_name','restaurant')

admin.site.register(Category, CategoryAdmin)

class OrderAdmin(admin.ModelAdmin):
	list_display = ('order_no', 'order_amount', 'restaurant')
	list_filter = ('order_no', 'order_amount', 'restaurant')
	search_fields = ('order_no', 'order_amount')

admin.site.register(Order, OrderAdmin)


class SectionAdmin(admin.ModelAdmin):
	list_display = ('section_name', 'section_rate')
	list_filter = ('section_name', 'section_rate')
	search_fields = ('section_name', 'section_rate')

admin.site.register(Section, SectionAdmin)


class TableAdmin(admin.ModelAdmin):
	list_display = ('table_no', 'description', 'section', 'order_id','order_amount')
	list_filter = ('table_no', 'description', 'section', 'order')
	search_fields = ('table_no', 'description')

	def order_id(self, obj):
		return obj.order.id

	def order_amount(self, obj):
		return obj.order.order_amount

admin.site.register(Table, TableAdmin)


class TableOrderMappingAdmin(admin.ModelAdmin):
	list_display = ('description', 'table', 'order_id','order_amount')
	list_filter = ('description', 'table','order')
	search_fields = ('description',)

	def order_id(self, obj):
		return obj.order.id

	def order_amount(self, obj):
		return obj.order.order_amount

admin.site.register(TableOrderMapping, TableOrderMappingAdmin)


class ItemOrderMappingAdmin(admin.ModelAdmin):
	list_display = ('item_name', 'item_rate', 'quantity', 'item', 'order_id','order_amount')
	list_filter = ('item_name', 'item_rate', 'item', 'order')
	search_fields = ('item_name', 'item_rate')

	def order_id(self, obj):
		return obj.order.id

	def order_amount(self, obj):
		return obj.order.order_amount

admin.site.register(ItemOrderMapping, ItemOrderMappingAdmin)


class BillAdmin(admin.ModelAdmin):
	list_display = ('id','bill_date', 'bill_amount','order_id')
	list_filter = ('id','bill_date', 'bill_amount', 'order')
	search_fields = ('bill_date', 'bill_amount')

	def order_id(self, obj):
		return obj.order.id

	def order_amount(self, obj):
		return obj.order.order_amount

admin.site.register(Bill, BillAdmin)


class CategoryTypeAdmin(admin.ModelAdmin):
	list_display = ('id', 'category_type')
	list_filter = ('id','category_type')
	search_fields = ('category_type',)

admin.site.register(CategoryType, CategoryTypeAdmin)


