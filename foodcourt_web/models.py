# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
import os, re
import json
from foodcourt_web.settings import BASE_DIR


class Restaurant(models.Model):
    admin = models.OneToOneField(User, null=True)
    restaurant_name = models.CharField(max_length=300)
    restaurant_address = models.CharField(max_length=300)
    restaurant_contact = models.IntegerField()

    created=models.DateTimeField(auto_now_add=True)
    modified=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{} {} {}".format(self.id, self.restaurant_name, self.restaurant_address,
                                       self.restaurant_contact
                                      )

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['admin_id'] = self.admin.id if self.admin else None
        result['first_name'] = self.admin.first_name if self.admin else None
        result['last_name'] = self.admin.last_name if self.admin else None
        result['restaurant_name'] = self.restaurant_name if self.restaurant_name else None
        result['restaurant_address'] = self.restaurant_address if self.restaurant_address else None
        result['restaurant_contact'] = self.restaurant_contact if self.restaurant_contact else None
        result['created'] = self.created if self.created else None
        result['modified'] = self.modified if self.modified else None

        print result
        return result

class Category(models.Model):
    category_name = models.CharField(max_length=300)
    icon = models.ImageField(null=True,blank=True)
    restaurant = models.ForeignKey(Restaurant)
    categorytype = models.ForeignKey('CategoryType')
    is_active = models.BooleanField()

    created=models.DateTimeField(auto_now_add=True)
    modified=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{} {}".format(self.id, self.category_name)

    def get_icon_and_name_and_type(self):
        result = {}
        result['category_name'] = self.category_name if self.category_name else None
        result['icon'] = str(self.icon) if self.icon else None
        return result

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['restaurant_id'] = self.restaurant.id if self.restaurant else None
        result['restaurant_name'] = self.restaurant.restaurant_name if self.restaurant else None
        result['categorytype_id'] = self.categorytype.id if self.categorytype else None
        result['category_name'] = self.category_name if self.category_name else None
        result['icon'] = str(self.icon) if self.icon else None
        result['created'] = self.created if self.created else None
        result['modified'] = self.modified if self.modified else None
        result['is_active'] = self.is_active if self.is_active else None

        print result
        return result

class Item(models.Model):
    item_name = models.CharField(max_length=200)
    item_code = models.CharField(max_length=200,unique=True)
    item_image = models.ImageField()
    category = models.ForeignKey('Category')
    item_rate = models.FloatField()
    restaurant = models.ForeignKey('Restaurant')

    created=models.DateTimeField(auto_now_add=True)
    modified=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{} {} {} {}".format(self.id, self.item_name, self.item_rate, self.item_code)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['restaurant_id'] = self.restaurant.id if self.restaurant else None
        result['restaurant_name'] = self.restaurant.restaurant_name if self.restaurant else None
        result['category_id'] = self.category.id if self.category else None
        result['category_name'] = self.category.category_name if self.category else None
        result['item_name'] = self.item_name if self.item_name else None
        result['item_image'] = str(self.item_image) if self.item_image else None
        result['item_rate'] = self.item_rate if self.item_rate else None
        result['item_code'] = self.item_code if self.item_code else None
        result['created'] = self.created if self.created else None
        result['modified'] = self.modified if self.modified else None

        print result
        return result

class Table(models.Model):
    table_no = models.CharField(max_length=100,unique=True)
    description = models.CharField(max_length=300, blank=True, null=True )
    restaurant = models.ForeignKey(Restaurant)
    order = models.ForeignKey("Order")
    section = models.ForeignKey("Section",null=True,blank=True)
    is_available = models.BooleanField(default = True)
    created=models.DateTimeField(auto_now_add=True)
    modified=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{} {} {} {}".format(self.id, self.table_no,self.description,
                                 self.is_available
                                )

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['restaurant_id'] = self.restaurant.id if self.restaurant else None
        result['restaurant_name'] = self.restaurant.restaurant_name if self.restaurant else None
        result['order_id'] = self.order.id if self.order else None
        result['order_no'] = self.order.order_no if self.order else None
        result['section_id'] = self.section.id if self.section else None
        result['section_name'] = self.section.section_name if self.section else None
        result['table_no'] = self.table_no if self.table_no else None
        result['description'] = self.description if self.description else None
        result['is_available'] = self.is_available if self.is_available else None
        result['created'] = self.created if self.created else None
        result['modified'] = self.modified if self.modified else None

        print result
        return result

class Order(models.Model):
    order_no = models.IntegerField(null=True,blank=True)
    order_amount = models.FloatField(default = 0)
    restaurant = models.ForeignKey(Restaurant)

    created=models.DateTimeField(auto_now_add=True)
    modified=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{} {} {}".format(self.id, self.order_no, self.order_amount)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['restaurant_id'] = self.restaurant.id if self.restaurant else None
        result['restaurant_name'] = self.restaurant.restaurant_name if self.restaurant else None
        result['order_no'] = self.order_no if self.order_no else None
        result['order_amount'] = self.order_amount if self.order_amount else None
        result['created'] = self.created if self.created else None
        result['modified'] = self.modified if self.modified else None

        print result
        return result

class Bill(models.Model):
    # bill_no = models.IntegerField(null=True, blank=True)
    bill_date = models.DateTimeField()
    bill_amount = models.FloatField()
    # bill_rate = models.FloatField()
    file = models.FileField()
    restaurant = models.ForeignKey(Restaurant)
    order = models.ForeignKey(Order)

    created=models.DateTimeField(auto_now_add=True)
    modified=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "ID: {} |Bill Date: {} |Total: {}".format(self.id, self.bill_date.strftime('%d-%m-%Y'), self.bill_amount)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['restaurant_id'] = self.restaurant.id if self.restaurant else None
        result['restaurant_name'] = self.restaurant.restaurant_name if self.restaurant else None
        result['order_id'] = self.order.id if self.order else None
        result['order_no'] = self.order.order_no if self.order else None
        # result['bill_no'] = self.bill_no if self.bill_no else None
        result['bill_date'] = self.bill_date if self.bill_date else None
        result['bill_amount'] = self.bill_amount if self.bill_amount else None
        # result['bill_rate'] = self.bill_rate if self.bill_rate else None
        result['file'] = self.file if self.file else None
        result['created'] = self.created if self.created else None
        result['modified'] = self.modified if self.modified else None

        print result
        return result

class ItemOrderMapping(models.Model):
    item_name = models.CharField(max_length=300)
    item_rate = models.FloatField()
    item_code = models.IntegerField()
    quantity = models.FloatField(default=0.00)
    order = models.ForeignKey(Order)
    item = models.ForeignKey(Item)

    created=models.DateTimeField(auto_now_add=True)
    modified=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return " {} {} {} {} {}".format(self.id, self.item_name, self.item_rate,self.item_code,self.quantity)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['order_id'] = self.order.id if self.order else None
        result['order_no'] = self.order.order_no if self.order else None
        result['item_id'] = self.item.id if self.item else None
        result['item_name'] = self.item.item_name if self.item else None
        result['item_rate'] = self.item_rate if self.item_rate else None
        result['item_code'] = self.item.item_code if self.item else None
        result['freezed_item_name'] = self.item_name if self.item_name else None
        result['quantity'] = self.quantity if self.quantity else None
        result['created'] = self.created if self.created else None
        result['order_amount'] = self.order.order_amount if self.order else None
        result['modified'] = self.modified if self.modified else None

        print result
        return result

class Section(models.Model):
    section_name = models.CharField(max_length=300)
    section_rate = models.FloatField()

    created=models.DateTimeField(auto_now_add=True)
    modified=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return " {} {} {}".format(self.id, self.section_name, self.section_rate)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['section_name'] = self.section_name if self.section_name else None
        result['section_rate'] = self.section_rate if self.section_rate else None
        result['created'] = self.created if self.created else None
        result['modified'] = self.modified if self.modified else None

        print result
        return result

class TableOrderMapping(models.Model):
    # table_no = models.CharField(max_length=100,unique=True)
    description = models.CharField(max_length=300, blank=True, null=True)
    order = models.ForeignKey(Order)
    table = models.ForeignKey(Table)

    created=models.DateTimeField(auto_now_add=True)
    modified=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return " {} {}".format(self.id, self.description)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['order_id'] = self.order.id if self.order else None
        result['order_no'] = self.order.order_no if self.order else None
        result['table_id'] = self.table.id if self.table else None
        result['table_no'] = self.table.table_no if self.table else None
        # result['table_no'] = self.table_no if self.table_no else None
        result['description'] = self.description if self.description else None
        result['created'] = self.created if self.created else None
        result['modified'] = self.modified if self.modified else None

        print result
        return result


class CategoryType(models.Model):
    category_type = models.CharField(max_length=200,unique=True)

    def __unicode__(self):
        return "{} {}".format(self.id, self.category_type)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['category_type'] = self.category_type if self.category_type else None
        # result['created'] = self.created if self.created else None
        # result['modified'] = self.modified if self.modified else None

        print result
        return result


class BillReport(models.Model):

    from_date = models.DateField()
    to_date = models.DateField()

    def __unicode__(self):
        return "{} {} {}".format(self.id,self.from_date,self.to_date)
    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['from_date'] = self.from_date if self.from_date else None
        result['to_date'] = self.to_date if self.to_date else None

        print result
        return result





