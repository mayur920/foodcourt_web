"""foodcourt_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from foodcourt_web.views import admin_login,admin_logout,get_restaurant_values,\
                                add_category,get_category,get_all_category,\
                                get_item,get_all_items,get_category_items,\
                                toggle_category,add_table,add_section,add_order,\
                                table_order_mapping,item_order_mapping,add_item,\
                                add_categorytype,get_all_tables,get_itemordermapping,\
                                get_tableordermapping,get_all_itemordermapping,\
                                get_all_tableordermapping,get_categorytype,\
                                get_all_categorytype,get_all_available_tables,\
                                get_all_unavailable_tables,toggle_table_availability,\
                                add_item_to_order, remove_item_from_order,create_bill,\
                                get_item_by_id,get_bill_by_date,get_all_sections



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', admin_login),
    url(r'^logout/$', admin_logout),
    url(r'^get/restaurant/values/$', get_restaurant_values),
    url(r'^add/category/$', add_category),
    url(r'^get/category/$', get_category),
    url(r'^get/all/category/$', get_all_category),
    url(r'^get/item/$', get_item),
    url(r'^get/all/items/$', get_all_items),
    url(r'^get/category/items/$', get_category_items),
    url(r'^toggle/category/$', toggle_category),
    url(r'^add/table/$', add_table),
    url(r'^add/section/$', add_section),
    url(r'^add/order/$', add_order),
    url(r'^table/order/mapping/$', table_order_mapping),
    url(r'^item/order/mapping/$', item_order_mapping),
    url(r'^add/item/$', add_item),
    url(r'^add/categorytype/$', add_categorytype),
    url(r'^get/all/tables/$', get_all_tables),
    url(r'^get/itemordermapping/$', get_itemordermapping),
    url(r'^get/tableordermapping/$', get_tableordermapping),
    url(r'^get/all/itemordermapping/$', get_all_itemordermapping),
    url(r'^get/all/tableordermapping/$', get_all_tableordermapping),
     url(r'^get/categorytype/$', get_categorytype),
     url(r'^get/all/categorytype/$', get_all_categorytype),
     url(r'^get/all/available/tables/$', get_all_available_tables),
     url(r'^get/all/unavailable/tables/$', get_all_unavailable_tables),
     url(r'^toggle/table/availability/$', toggle_table_availability),
     url(r'^add/item/to/order/$', add_item_to_order),
     url(r'^remove/item/from/order/$', remove_item_from_order),
     url(r'^create/bill/$', create_bill),
     url(r'^get/item/by/id/$', get_item_by_id),
     url(r'^get/bill/by/date/$', get_bill_by_date),
     url(r'^get/all/sections/$', get_all_sections)
]

