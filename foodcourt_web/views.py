from foodcourt_web.models import Restaurant,Category,Item,Table,Order,Bill,ItemOrderMapping,Section,TableOrderMapping,CategoryType,BillReport
from django.http import JsonResponse, HttpResponse
from django.db.models import Q
from django.db import transaction
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import login,authenticate,logout
import json, datetime, calendar
from django.core.management.base import CommandError


def convert_date_to_epoch(date):
    return int(date.strftime('%s'))*1000 if date else None


def convert_epoch_to_date(epoch):
    return datetime.datetime.fromtimestamp(long(epoch)/1000.0) if epoch else None

def admin_login(request):
    json_obj = json.loads(request.body)
    username = json_obj.get('username')
    password = json_obj.get('password')

    print json_obj
    if not username:
        return JsonResponse({"validation": "Please enter a valid username..!!", "status": False})

    admin = authenticate(username=username, password=password)

    if not admin:
        print 'Admin not found in db'
        return JsonResponse({"validation": "Invalid Admin", "status": False})

    if not admin.is_active:
        print 'admin is inactive'
        return JsonResponse({"validation": "Invalid Admin", "status": False})

    login(request, admin)

    return JsonResponse({"validation": "Login Successful", "status": True})

def admin_logout(request):
    logout(request)

    return JsonResponse({"validation":"Logout successfully", "status":True})

def get_restaurant_values(request):
    json_obj =json.loads(request.body)
    restaurant_obj = Restaurant.objects.get(admin=request.user)
    restaurant_json_object = restaurant_obj.get_json()
    return JsonResponse({"data": restaurant_json_object, "status": True})

def add_category(request):
    json_obj = json.loads(request.body)
    input_categorytype_id = json_obj.get("categorytypeId")
    input_category_name = json_obj.get("categoryName")
    is_active = json_obj.get("isActive")
    input_icon = json_obj.get("icon")

    restaurant_obj = Restaurant.objects.get(admin=request.user)
    categorytype_obj = CategoryType.objects.get(id=input_categorytype_id)
    print input_category_name, input_icon

    if not input_category_name:
        return JsonResponse({"validation": "Enter Category Name ", "status": False})

    try:
        category_obj = Category.objects.create(category_name=input_category_name,
                                            icon=input_icon,
                                            restaurant=restaurant_obj,
                                            categorytype=categorytype_obj,
                                            is_active=is_active
                                            )
    except Exception as e:
        print e
    return JsonResponse({"validation": "Category Info saved successfully", "status": True})

def get_category(request):
    json_obj = json.loads(request.body)
    category_id = json_obj.get("categoryId")
    category_obj = Category.objects.get(id=category_id)
    category_json_object = category_obj.get_icon_and_name_and_type()

    return JsonResponse({"data": category_json_object, "status": True})

def get_all_category(request):
    all_category_list=[]
    categories = Category.objects.all()
    for category in categories:
        all_category_list.append(category.get_json())
    return JsonResponse({"data": all_category_list, "status": True})

def get_item(request):
    json_obj = json.loads(request.body)
    item_id = json_obj.get("itemId")
    item_obj = Item.objects.get(id=item_id)
    item_json_object = item_obj.get_json()

    return JsonResponse({"data":item_json_object, "status": True})

def get_all_items(request):
    all_item_list=[]
    items = Item.objects.all()
    for item in items:
        all_item_list.append(item.get_json())
    return JsonResponse({"data":all_item_list, "status": True})

def get_category_items(request):
    json_obj=json.loads(request.body)
    category_id = json_obj.get("categoryId")

    print 'getting item from the category which has id: ', category_id

    items = Item.objects.filter(category__id = category_id)

    all_item_list=[]

    for item in items:
        all_item_list.append(item.get_json())

    return JsonResponse({"data": all_item_list, "status": True})

def toggle_category(request):
    json_obj = json.loads(request.body)
    category_id = json_obj.get("category_id")

    try:
        category = Category.objects.get(id = category_id)
        if category.is_active:
            category.is_active == False
            response_message = 'Category disabled'
        elif not category.is_active:
            category.is_active == True
            response_message = 'Category enabled'

        category.save()
    except Exception as e:
        print e
        return JsonResponse({"validation": "Invalid category", "status": False})

    return JsonResponse({"validation": response_message, "status": True})


def add_table(request):
    json_obj = json.loads(request.body)
    input_order_id = json_obj.get("orderId")
    input_section_id = json_obj.get("sectionId")
    input_table_no = json_obj.get("tableNo")
    input_description = json_obj.get("description")


    restaurant_obj = Restaurant.objects.get(admin=request.user)
    order_obj = Order.objects.get(id=input_order_id)
    section_obj = Section.objects.get(id=input_section_id)
    print input_table_no, input_description

    table_obj = Table.objects.filter(table_no=input_table_no)
    if table_obj:
        return JsonResponse({"validation": "Table Number already exist with this code", "status": False})

    if not input_description:
        return JsonResponse({"validation": "Enter Description", "status": False})
    try:
        table_obj = Table.objects.create(table_no=input_table_no,
                                        description=input_description,
                                        restaurant=restaurant_obj,
                                        order=order_obj,
                                        section=section_obj
                                        )
    except Exception as e:
        print e
    return JsonResponse({"validation": "Table Info saved successfully", "status": True})


def add_section(request):
    json_obj = json.loads(request.body)
    input_section_name = json_obj.get("sectionName")
    input_section_rate = json_obj.get("sectionRate")

    print input_section_name, input_section_rate

    if not input_section_name:
        return JsonResponse({"validation": "Enter Section Name ", "status": False})
    try:
        section_obj = Section.objects.create(section_name=input_section_name,
                                            section_rate=input_section_rate)
    except Exception as e:
        print e
    return JsonResponse({"validation": "Section Info saved successfully", "status": True})

def get_all_sections(request):
    all_section_list=[]
    sections = Section.objects.all()
    for section in sections:
        all_section_list.append(section.get_json())
    return JsonResponse({"data":all_section_list, "status": True})

def add_order(request):
    if not request.user.is_authenticated():
        return JsonResponse({"validation": "Invalid request", "status": False})

    json_obj = json.loads(request.body)
    input_table_id = json_obj.get("tableId")

    print input_table_id

    restaurant_obj = Restaurant.objects.get(admin=request.user)

    table_obj = Table.objects.get(id=input_table_id)

    with transaction.atomic():
        order_obj = Order.objects.create(restaurant=restaurant_obj)
        order_obj.order_no = order_obj.id
        order_obj.save()
        table_order_mapping_obj = TableOrderMapping.objects.create(table=table_obj, order=order_obj)

    return JsonResponse({"data": order_obj.id, "status": True})


def table_order_mapping(request):
    json_obj = json.loads(request.body)
    input_order_id = json_obj.get("orderId")
    input_table_id = json_obj.get("tableId")
    input_description = json_obj.get("description")

    order_obj = Order.objects.get(id=input_order_id)
    table_obj = Table.objects.get(id=input_table_id)


    print  input_description

    table_order_mapping_obj = TableOrderMapping.objects.filter(table=table_obj,order=order_obj)
    if table_order_mapping_obj:
        return JsonResponse({"validation": "Table Number already exist with this code", "status": False})


    if not input_description:
        return JsonResponse({"validation": "Enter Description", "status": True})

    try:
        table_order_mapping_obj = TableOrderMapping.objects.create(
                                            description=input_description,
                                            table=table_obj,
                                            order=order_obj
                                            )
    except Exception as e:
        print e
    return JsonResponse({"validation": "Table-Order Info saved successfully", "status": True})


def item_order_mapping(request):
    json_obj = json.loads(request.body)
    input_order_id = json_obj.get("orderId")
    input_item_id = json_obj.get("itemId")
    input_item_name = json_obj.get("itemName")
    input_item_rate = json_obj.get("itemRate")
    input_quantity = json_obj.get("quantity")
    input_item_code = json_obj.get("itemCode")

    order_obj = Order.objects.get(id=input_order_id)
    item_obj = Item.objects.get(id=input_item_id)


    print input_item_name, input_item_rate, input_quantity,input_item_code

    if not input_item_name:
        return JsonResponse({"validation": "Enter Item Name", "status": True})

    try:
        item_order_mapping_obj = ItemOrderMapping.objects.create(item_name=input_item_name,
                                            item_rate=input_item_rate,quantity=input_quantity,
                                            order=order_obj,item=item_obj,item_code=input_item_code
                                            )

    except Exception as e:
        print e
    return JsonResponse({"validation": "Item-Order Info saved successfully", "status": True})


def add_item(request):
    if not request.user.is_authenticated():
        return JsonResponse({"validation": "Invalid request", "status": False})

    json_obj = json.loads(request.body)
    input_category_id = json_obj.get("categoryId")
    input_item_name = json_obj.get("item_name")
    input_item_code = json_obj.get("item_code")
    input_item_image = json_obj.get("item_image")
    input_item_rate = json_obj.get("item_rate")

    try:
        restaurant_obj = Restaurant.objects.get(admin=request.user)
    except Exception as e:
        print e
        return JsonResponse({"validation": "Invalid request", "status": False})


    category_obj = Category.objects.get(id=input_category_id)

    print input_item_name, input_item_code,input_item_image,input_item_rate

    item_obj = Item.objects.filter(item_code=input_item_code)
    if item_obj:
        return JsonResponse({"validation": "Item code already exist with this code", "status": False})
    try:
        item_obj = Item.objects.create(item_name=input_item_name,
                                        item_code=input_item_code,
                                        item_image=input_item_image,
                                        item_rate=input_item_rate,
                                        restaurant=restaurant_obj,
                                        category=category_obj
                                    )
    except Exception as e:
        print e
    return JsonResponse({"validation": "Item Info saved successfully", "status": True})

def add_categorytype(request):
    json_obj = json.loads(request.body)
    input_category_type = json_obj.get("categoryType")

    if not input_category_type:
        return JsonResponse({"validation": "Enter Category Type ", "status": False})

    categorytype_obj = CategoryType.objects.filter(category_type=input_category_type)

    if categorytype_obj:
        return JsonResponse({"validation": "Category type already exist with this name", "status": False})

    categorytype_obj = CategoryType.objects.create(category_type=input_category_type)

    return JsonResponse({"validation": "CategoryType Info saved successfully", "status": True})


def get_all_tables(request):
    all_table_list=[]
    tables = Table.objects.all()
    for table in tables:
        all_table_list.append(table.get_json())
    return JsonResponse({"data":all_table_list, "status": True})


def get_itemordermapping(request):
    json_obj = json.loads(request.body)
    order_id = json_obj.get("orderId")
    itemordermappings = ItemOrderMapping.objects.filter(order__id=order_id).order_by('-id')

    item_order_list = [itemordermapping_obj.get_json() for itemordermapping_obj in itemordermappings]

    return JsonResponse({"data": item_order_list, "status": True})

def get_tableordermapping(request):
    json_obj = json.loads(request.body)
    tableordermapping_id = json_obj.get("tableordermappingId")
    tableordermapping_obj = TableOrderMapping.objects.get(id=tableordermapping_id)
    tableordermapping_json_object = tableordermapping_obj.get_json()

    return JsonResponse({"data":tableordermapping_json_object, "status": True})


def get_all_itemordermapping(request):
    all_itemordermapping_list=[]
    itemordermappings = ItemOrderMapping.objects.all()
    for itemordermapping in itemordermappings:
        all_itemordermapping_list.append(itemordermapping.get_json())
    return JsonResponse({"data":all_itemordermapping_list, "status": True})


def get_all_tableordermapping(request):
    all_tableordermapping_list=[]
    tableordermappings = TableOrderMapping.objects.all()
    for tableordermapping in tableordermappings:
        all_tableordermapping_list.append(tableordermapping.get_json())
    return JsonResponse({"data":all_tableordermapping_list, "status": True})


def get_categorytype(request):
    json_obj = json.loads(request.body)
    categorytype_id = json_obj.get("categorytypeId")
    categorytype_obj = CategoryType.objects.get(id=categorytype_id)
    categorytype_json_object = categorytype_obj.get_json()
    return JsonResponse({"data": categorytype_json_object, "status": True})


def get_all_categorytype(request):
    all_categorytype_list=[]
    categorytypes = CategoryType.objects.all()
    for categorytype in categorytypes:
        all_categorytype_list.append(categorytype.get_json())
    return JsonResponse({"data":all_categorytype_list, "status": True})


def get_all_available_tables(request):
    table_list=[]
    tables = Table.objects.filter(is_available=True)
    for table in tables:
        table_list.append(table.get_json())
    return JsonResponse({"data":table_list, "status": True})

def get_all_unavailable_tables(request):
    table_list=[]
    tables = Table.objects.filter(is_available=False)
    for table in tables:
        table_list.append(table.get_json())
    return JsonResponse({"data":table_list, "status": True})

def toggle_table_availability(request):
    json_obj = json.loads(request.body)
    table_id = json_obj.get("tableId")
    try:
        table = Table.objects.get(id = table_id)
        if table.is_available:
            table.is_available = False
            response_message = 'Table booked'
        elif not table.is_available:
            table.is_available = True
            response_message = 'Table open'

        table.save()
    except Exception as e:
        print e
        return JsonResponse({"validation": "Invalid table", "status": False})

    return JsonResponse({"validation": response_message, "status": True})


def add_item_to_order(request):
    json_obj = json.loads(request.body)
    input_item_id = json_obj.get("itemId")
    input_table_id = json_obj.get("tableId")
    input_order_id = json_obj.get("orderId")
    input_quantity = float(json_obj.get("quantity", 0)) if json_obj.get("quantity") else 0

    if input_quantity <= 0:
        return JsonResponse({"validation": "Invalid Quantity", "status": False})

    item_obj = Item.objects.get(id=input_item_id)
    table_obj = Table.objects.get(id=input_table_id)
    order_obj = Order.objects.get(id=input_order_id)

    item_order_mappings = ItemOrderMapping.objects.filter(item=item_obj, order=order_obj)
    if item_order_mappings:
        item_order_mapping_obj = item_order_mappings.first()
    else:
        item_order_mapping_obj = None

    print input_item_id, input_table_id,input_order_id,input_quantity
    with transaction.atomic():
        if item_order_mapping_obj:
            item_order_mapping_obj.quantity += input_quantity
            item_order_mapping_obj.save()
        else:
            item_order_mapping_obj = ItemOrderMapping.objects.create(quantity=input_quantity,
                                                                    item_rate =item_obj.item_rate,
                                                                    item=item_obj,
                                                                    order=order_obj,
                                                                    item_name=item_obj.item_name,
                                                                    item_code=item_obj.item_code
                                                                    )

        order_obj.order_amount += input_quantity * item_order_mapping_obj.item_rate
        order_obj.save()

        table_order_mapping_obj = TableOrderMapping.objects.filter(table=table_obj, order=order_obj)

        if not table_order_mapping_obj:
            table_order_mapping_obj = TableOrderMapping.objects.create(table=table_obj, order=order_obj)

    return JsonResponse({"validation": "Item_Order_Mapping Info saved successfully", "status": True})


def remove_item_from_order(request):
    json_obj = json.loads(request.body)
    input_item_id = json_obj.get("itemId")
    input_order_id = json_obj.get("orderId")
    quantity = json_obj.get("quantity")

    item_obj = Item.objects.get(id=input_item_id)
    order_obj = Order.objects.get(id=input_order_id)

    if quantity:
        item_order_mapping_obj = ItemOrderMapping.objects.filter(item=item_obj, order=order_obj).first()
        quantity_to_be_removed = quantity
        item_rate = item_order_mapping_obj.item_rate

        if item_order_mapping_obj.quantity > quantity:
            item_order_mapping_obj.quantity -= quantity
            item_order_mapping_obj.save()
        else:
            item_order_mapping_obj.delete()

    else:
        try:
            item_order_mapping_obj = ItemOrderMapping.objects.filter(item=item_obj, order=order_obj).first()
            quantity_to_be_removed = item_order_mapping_obj.quantity
            item_rate = item_order_mapping_obj.item_rate
            item_order_mapping_obj.delete()
        except Exception as e:
            print e
            return JsonResponse({"validation": "Error Occured", "status": False})

    order_obj.order_amount -= item_rate *quantity_to_be_removed
    order_obj.save()

    return JsonResponse({"validation": " Item removed from order", "status": True})


def create_bill(request):
    json_obj = json.loads(request.body)
    input_order_id = json_obj.get("orderId")
    input_amount = json_obj.get("amount")
    bill_date = convert_epoch_to_date(json_obj.get("billDate")) if json_obj.get("billDate") else datetime.datetime.today()

    order_obj = Order.objects.get(id=input_order_id)
    restaurant = Restaurant.objects.get(admin=request.user)

    print input_order_id, input_amount, bill_date

    item_order_mappings = ItemOrderMapping.objects.filter(order=order_obj)

    total_amount = 0

    for item_order_mapping in item_order_mappings:
        print 'item_order_mapping.item_rate: ', item_order_mapping.item_rate
        print 'item_order_mapping.quantity: ', item_order_mapping.quantity
        total_amount += item_order_mapping.item_rate * item_order_mapping.quantity

    print 'total_amount: ', total_amount
    if int(total_amount) != int(input_amount):
        return JsonResponse({"validation": "Amount does not matched", "status": False})

    bill_obj = Bill.objects.create(bill_amount=input_amount, order=order_obj,
                                   bill_date=bill_date,restaurant=restaurant)

    return JsonResponse({"validation": "Bill information saved successfully", "status": True})


def get_item_by_id(request):
    json_obj = json.loads(request.body)
    item_id = json_obj.get("itemId")
    item_obj = Item.objects.get(id=item_id)

    return JsonResponse({"data": item_obj.get_json(), "status": True})


def get_bill_by_date(request):
    json_obj = json.loads(request.body)

    input_from_date = json_obj.get("fromDate")
    input_to_date = json_obj.get("toDate")

    from_date = convert_epoch_to_date(input_from_date)
    to_date = convert_epoch_to_date(input_to_date)

    print from_date, to_date

    all_bill_list=[]
    bills = Bill.objects.filter(created__range=(from_date, to_date))
    for bill in bills:
        all_bill_list.append(bill.get_json())
    return JsonResponse({"data":all_bill_list, "status": True})












